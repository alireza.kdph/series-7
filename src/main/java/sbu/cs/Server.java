package sbu.cs;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    /**
     * Create a server here and wait for connection to establish,
     *  then get file name and after that get file and save it in the given directory
     *
     * @param args an string array with one element
     * @throws IOException
     */

    private static ServerSocket server;
    private static Socket socket;
    ;
    static {
        try {
            server = new ServerSocket(7000);
        } catch (IOException e) {
        }
    }

    public static void main(String[] args) throws IOException {
        // below is the name of directory which you must save the file in it
        String directory = args[0];     // default: "server-database"
        socket = server.accept();
        DataInputStream in = new DataInputStream(socket.getInputStream());
        String filePath = in.readUTF();
        FileOutputStream fOutput = new FileOutputStream(directory + "/" + filePath);
        int len = in.readInt();
        byte[] file = new byte[len];
        receiveFile(file, in);
        createFile(fOutput, file);
        fOutput.close();
        in.close();
        socket.close();
        server.close();
    }

    private static void createFile(FileOutputStream fOutput, byte[] file) throws IOException {
        fOutput.write(file);
    }

    private static void receiveFile(byte[] file, DataInputStream in) throws IOException {
        in.readFully(file);
    }
}
