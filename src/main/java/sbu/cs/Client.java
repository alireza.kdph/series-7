package sbu.cs;

import javax.xml.crypto.Data;
import java.io.*;
import java.net.Socket;

public class Client {

    /**
     * Create a socket and connect to server then send fileName and after that send file to be saved on server side.
     *  in here filePath == fileName
     *
     * @param args a string array with one element
     * @throws IOException
     */
    
    private static Socket socket;
    private static DataOutputStream out;
    static {
        try {
            socket = new Socket("localhost", 7000);
            out = new DataOutputStream(socket.getOutputStream());

        } catch (IOException e) {
        }
    }

    ;
    public static void main(String[] args) throws IOException {
        String filePath = args[0];      // "sbu.png" or "book.pdf"
        DataInputStream fInput = new DataInputStream(new FileInputStream(filePath));
        byte[] file = new byte[fInput.available()];
        readFile(fInput, file);
        out.writeUTF(filePath);
        out.writeInt(file.length);
        sendFile(file, out);
        out.close();
        fInput.close();
        socket.close();
    }

    private static void readFile(DataInputStream fInput, byte[] file) throws IOException {
        fInput.readFully(file);
    }

    private static void sendFile(byte[] file, DataOutputStream out) throws IOException {
        out.write(file);
    }
}

